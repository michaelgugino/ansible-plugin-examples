# Lesson 3: Action plugins

## About
The two main units of 'doing things' in ansible are Actions and Modules.

Actions are executed on the localhost (the host running ansible).  Modules
are executed remotely against the target hosts.

Some examples of actions are:
1. The 'debug' task
2. The 'set_fact' task

Both of those items don't require connecting to the remote host to do any
work; they are executed locally.

Some actions call modules.  For example, the 'package' task is actually
an action that calls the appropriate module and executes that module against
the remote host.  Check it out:
https://github.com/ansible/ansible/blob/stable-2.4/lib/ansible/plugins/action/package.py

That action serves as a really important reference to how to properly write
action plugins, IMO.  Of course, that action calls a module, but not all
actions have to call a module.

Similar to filters, it's important that anything needed to execute an action
should be installed on the localhost before the action is executed.

## Let's Code

Here's a useless, bare-minimum (almost, display is not required) action plugin:
```python
# roles/meta_role_1/action_plugins/demo_action1.py
from ansible.plugins.action import ActionBase
from ansible import errors

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

class ActionModule(ActionBase):
    """A completely useless action"""
    def run(self, tmp=None, task_vars=None):
        """Run demo_action1 action plugin"""
        display.v("Running useless action: v")
        display.vvv("Running useless action: vvv")
        return {'changed': True, 'failed': False, 'msg': "I'm useless!"}
```

An action needs to have the following:
1. Source file needs to share the same name as what you'll put in a task, eg 'demo_action1'
2. Must implement a child class of "ActionBase" called "ActionModule"
3. ActionModule must have an instance method "run" with the above signature, eg needs all those params.
4. Should return a dictionary consisting of "changed", "failed", and "msg", at minimum.

The really nice thing about actions is you can return more than just the standard
dictionary if you choose.  For example:

```python
return {
    'changed': True,
    'failed': False,
    'msg': "I'm useless!",
    'some_data': "something useful"}

```
If we modify the returned dictionary from above, save it as
demo_action2.py, we can access some_data like so:

```yaml
- demo_action2: {}
  register: demo_action2_res
- debug:
    var: demo_action2_res['some_data']
```

This is really useful for building complicated data structures for later use,
or for storing results from APIs, and so forth.

## Parameters and Variables
You are probably aware, not may actions or modules take 0 parameters.

Let's see how we should access proper task parameters.

```python
def run(self, tmp=None, task_vars=None):
    """Run demo_action3 action plugin"""

    # Need to run parent class's init method.
    result = super(ActionModule, self).run(tmp, task_vars)

    # Easy enough to access parameters, like so:
    param1 = self._task.args.get('param1', 'parma1_default')
    param2 = self._task.args.get('param2', 'parma2_default')

    # We can access variables that are not parameters as well.
    var1 = task_vars.get('tvar1')
    var2 = task_vars.get('tvar2')

    # You pretty much always want to template variables that you access
    # via task_vars.get...
    var3 = self._templar.template(var2)

    msg = "It worked!"
    return {'changed': True, 'failed': False, 'msg': msg,
            'param1': param1, 'param2': param2, 'var1': var1,
            'var2': var2, 'var3': var3}
```

As far as actions are concerned, there isn't a lot of boiler plate required
for passing in parameters.

The above can be used like so:
```yaml
vars:
  base_var: "This is a base string"
  tvar1: "This is tvar1"
  tvar2: "This is tvar2 and {{ tvar1 }}"
...
- demo_action3:
    param1: "I am the first param!"
    param2: "{{ base_var }}"
```

You can see the following output (from running exercise3.yml)
```sh
changed: [localhost] => {
    "changed": true,
    "msg": "It worked!",
    "param1": "I am the first param!",
    "param2": "This is a base string",
    "var1": "This is tvar1",
    "var2": "This is tvar2 and {{ tvar1 }}",
    "var3": "This is tvar2 and This is tvar1"
}
```

Notice, "var2" above is probably not what we want, be sure to template!

## Run Exercise
```sh
ansible-playbook exercise3.yml -i inventory/i.txt -vvv
```

## Learn more...

Need to verify a bunch of stuff in a user's inventory, but don't want to create
a bunch of tasks?

https://github.com/openshift/openshift-ansible/blob/master/roles/lib_utils/action_plugins/sanity_checks.py
