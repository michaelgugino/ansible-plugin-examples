# Setting up the dev environment

See README.md for how to install ansible, preferably into a virtualenv.

When developing roles and plays, I like to just create a simple directory
that contains some plays I want to test, and a folder with roles.  The best
way to test lots of things in ansible is to actually run the plays.  In fact,
that's what ansible itself does to test many of the modules in the CI/CD
pipeline.

# Pro Tip: version control
Your roles and plays belong in version control and ideally you have a CI/CD
pipeline to test changes to your roles and plays.  Treat them very much like
'software' with as little configuration data as possible.

Your inventory and it's associated group and host vars
belong in a separate version control repository from your roles and plays.
Inventory + group_vars + host_vars are your 'configuration data' and should
be kept separate from your 'software'.

In the case of a dynamic inventory script, it's helpful to still keep host_vars
and group_vars in inventory, and try to keep your dynamic script's responsibility
limited to hosts and their groupings.

# Some notes on plays, playbooks, task files, and scope.

A 'playbook' contains one or more 'plays' and or import statements
for other playbooks.

## Playbooks and plays
Example 1-1:

```yaml
---
# Example 1-1
# This whole file would be a 'playbook'

# Our first 'play,' 'Play1'
- name: Play1
  hosts: some_group
  vars:
    - play1_var: "This is play1"
  tasks:
    - debug:
        var: play1_var

# Our Second 'play,' 'Play2'
- name: Play2
  hosts: some_other_group
  tasks:
    - debug:
        var: play1_var
```

Plays live inside of playbooks, eg hello_word.yml.  Playbooks can import other
playbooks, not task files.

## Task files
Task files are imported by plays.  Task files are a collection of only tasks.

Example 1-2:

```yaml
---
- debug:
    msg: "I'm in a task file"
- debug:
    msg: "I'm another task"
```

The easiest way to determine if you are looking at a 'task file' or 'playbook'
is to look for a 'hosts:' line.  All plays need a 'hosts:' line.  Without that,
it can't be a play.

## Play Scope
IMO, play scope is one of the most confusing aspects of ansible.

Scope affects roles, variables, and plugins.

### play scope: roles
Role scope is probably the most confusing aspect of how ansible works.

Roles are brought into scope by the 'roles:' section of a play.

eg Example 1-3:
```yaml
---
- name: play 1
  hosts: all
  roles:
    - role: role1
    - role: role2

- name: play 2
  hosts: all
  roles:
    - role: role3
```

When a role is brought into scope, that roles defaults, vars/main.yml, and
plugins are brought into the play scope.  Additionally, the meta-dependencies
for each role are resolved on a play level.

What that means is if role1 defines the variable "role_1_var_1", role2
could directly use that same variable without defining it itself, as long as
role1 is available in the same play scope.  In fact, there is no requirement
that role1 be executed first; simply having a role 'in play scope' imports
all of it's plugins and variables for use across the entire play (not playbook).

In our example 1-3 above, role2 could 'see' all of role1's variables, but
role3 could not because role3 is in a separate play.

Often times, this situation of 'shared variables' across roles can result in
unwanted side effects if the roles are changed later, or there is another
play that calls role2 without having role1 in scope.  It's best not to 'share'
variables in this manner.  This situation can also lead users/developers to
use the meta/main.yml of a role to enforce a dependency.  This is also not
ideal.

Meta dependencies, dependencies which are listed in a role's meta/main.yml file,
are resolved only during a play's scope.  In example 1-3 above, if role1, role2,
and role3 all have a meta dependency of 'role_common', executing the
example 1-3 playbook would result in role_common executing twice: once in play 1,
and another time in play 2.  This is often times undesired.

However, meta dependencies are not always bad, and we'll discuss ways to
use them later.

### play scope: variables
There are not many ways to persist information 'across' plays.  That is to say,
in our example 1-3, play 2 does not have much information about the variables
provided in play 1.

The best way to persist variables across plays is with inventory variables.
Inventory variables override role defaults (and nothing else), and are utilized
in each play of a playbook.

Another way to persist information across plays is to use 'extra_vars', but
these have certain draw backs and should be limited and used only when 100%
necessary.

Sometimes, in really complicated setups, it's helpful to derive some information
dynamically, and save that information for later.  This is most done with the
task 'set_fact'.  Any variable set with 'set_fact' will persist across plays
for the entire ansible-playbook run.  Think carefully about setting items with
'set_fact' as you don't want to create undocumented dependencies across roles.

Additionally, using meta-dependencies to ensure that a role dependency is
dynamically resolved can cause numerous unwanted executions of the same role
across plays.  It's best to explicitly add whatever prerequisite roles at the
top of each playbook where they are needed.

Example 1-4:
```yaml
---
- name: play 0
  hosts: all
  roles:
    - role: set_a_bunch_of_facts_role

- name: play 1
  hosts: all
  roles:
    - role: role1
```

### play scope: plugins
Plugins (ie, custom filters, modules, actions, etc) can be shipped with roles,
in a directory relative to a playbook's directory, some where in /etc/ansible,
somewhere configured by ansible.cfg, etc.  It's really quite confusing.

This section is about plugins that ship with roles.  Plugins included in a role's
source can be used by other roles within the same play scope.

Knowing this, it's highly useful to create task-less roles that ship plugins
and default variables.  You can import these roles directly in a play, or
include them in another role's meta-dependencies.

# Exercise 1:
1. Examine the source of exercise1.yml.
2. Examine the source of role ex1_1, ex1_2, and ex1_3.
3. Predict the output of of 'play 2's debug statement.
4. Predict how many times, if any, ex1_3 will run (per host).
4. Execute exercise1.yml and verify your findings.
```
ansible-playbook -i inventory/i.txt exercise1.yml
```
