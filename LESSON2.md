# Lesson 2: Filter plugins

## About
http://docs.ansible.com/ansible/latest/playbooks_filters.html

Ansible utilizes many filters.  These filters are able to transform
data in a variety of ways.

For example, the default filter:
```yaml
x: {{ some_variable | default(5) }}
```

In the example above, we're setting x to the value of some_variable, or if
some_variable is undefined, we'll use the integer value 5.

Often times, working with various APIs and configuration files can require you
data to be ordered in a particular format.  Filters can really help adjust
your data and take burden off of your users by allowing users to define
simple variable types that you transform later.  Other times, you might
source some data from an API and it needs a lot of parsing to be useful.

Whatever the case, using filters is very common in sophisticated roles and
plays.


## Import Notes

Filters are executed on the host that is running ansible (aka, the deployment
host).  Filters are not actually executed until a variable is used.  For
example, you can define variables in a role's defaults which use a filter,
and the filter won't be executed until that variable is accessed by some task
or action.

Since filters run on the localhost, it's important to keep in mind any
non-standard python libraries must be installed into your localhost's
python environment.  This is a great reason to include a requirements.txt
in your ansible project, and always install your ansible projects into a
virtualenv.

## Let's Code

Let's look at our example filter code:
```python
# roles/meta_role_1/filter_plugins/filter.example.py
def meta_role_filter1(data="Empty?"):
    """Append something special to a string"""
    return str(data) + " Ansible rocks!"

class FilterModule(object):
    """ Custom ansible filter mapping """
    def filters(self):
        """ returns a mapping of filters to methods """
        return {"meta_role_filter1": meta_role_filter1}
```
That's the whole source of our filter plugin.

All we need to provide are:
1. A class of type "FilterObject"
2. "FilterObject" class has an instance method of "filters"
3. "filters" instance method returns a dictionary mapping of filter names to
function names found inside that source file.

The filename of the filter_plugin doesn't matter.  You can have more than
one source file containing one or more filter plugins as well.

We can use the filter like so:
```yaml
x: "{{ 'this is a string' | meta_role_fitler1 }}"
```

Filters can take multiple arguments:

```python
def meta_role_filter2(data="None?", other_var="none?")
```

And can be used like so:
```
x: "{{ 'first' | meta_role_filter2('second') }}"
```

## Shipping filters
Filters can easily be embedded in a role.  That is my preferred way of sharing
filters across various roles and plays.

Take a look at the playbook for exercise2:
```yaml
---
- name: play 1
  connection: local
  hosts: all
  vars:
    base_var: "This is a base string"
    ex2_var1: "{{ base_var | meta_role_filter1 }}"
    ex2_var2: "{{ 'first' | meta_role_filter2('second') }}"
  roles:
    - role: meta_role_1
  tasks:
    - debug:
        var: ex2_var1
    - debug:
        var: ex2_var2
```

If you examine the 'meta_role_1' role, you'll see that it only provides
a plugins and no tasks.  Essentially, we are
treating this role like a shared library.  It's very handy to have a
plugins-only role that can be included in plays or via meta-depends of other
roles.  This let's you ensure the plugins are always available without
running unnecessary tasks.

Checkout https://github.com/openshift/openshift-ansible/tree/master/roles/lib_utils

## Run exercise2
```sh
ansible-playbook exercise2.yml -i inventory/i.txt
```
