# Lesson 4: Module plugins

## About

This is where the real magic happens.  Modules run on the target hosts and
do useful things idempotently (hopefully).

I typically write fewer modules than filters and actions; I find ansible has
a very good amount of generally-useful modules that I can string together to
perform complex actions.

Sometimes stringing together commands in ansible is not ideal, not practical,
or just not efficient in a large environment.  You might want to create a
module to solve one of these problems.

Take a look at the apache_module module: https://github.com/ansible/ansible/blob/stable-2.4/lib/ansible/modules/web_infrastructure/apache2_module.py

There's not much magic there.  It's just running some commands on the remote
machine to determine if a module is enabled/disabled, and to take the
appropriate action.

## Let's Code

Let's examine roles/meta_role_1/library/demo_module1.py

```python
#!/usr/bin/env python

from ansible.module_utils.basic import AnsibleModule

...

def main():
    '''Run this module'''
    # Boiler plate for defining params.
    module_args = dict(
        url=dict(aliases=['url', 'api'], required=True, type='str'),
        username=dict(type='str', required=False, no_log=True),
        password=dict(type='str', required=False, no_log=True)
    )
    # Need to instantiate a module object
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=False
    )
    run(module=module)

if __name__ == '__main__':
    main()

```

This is the basic module boiler plate.  It requires:
1. Needs a proper interpreter as the first line.  env python usually suffices.
2. Need to import the AnsibleModule class.
3. Needs to be runnable like a script; ansible will copy over this file and
execute it.
4. Need to instantiate an instance of AnsibleModule.

Those are pretty much the requirements, and it's somewhat self explanatory
what's going on with the module object.

Let's take a look at what the module does:

```python
def run(module=None):
    # Access parameters
    myurl = module.params['url']
    result, stdout, stderr = module.run_command("ping -c1 %s" % myurl)
    if result:
        result = {'msg': stderr, 'failed': True, 'changed': False}
        # if you need to quick-exit and fail.
        module.fail_json(**result)
    msg = stdout
    msg += "; It worked! I pinged: {}".format(myurl)
    # This dictionary is for convenience
    result = {'changed': False, 'failed': False,
              'msg': msg}

    # how to exit a module the normal way.
    # **x means to 'unpack' a dictionary into keywords.
    # IE, (changed=False, failed=False, msg="...")
    module.exit_json(**result)
```

Above, I've shown some common methods that are built into the module class.

Similar to actions, module source files must be the name of the module,
and in the case of modules, they must be in the 'library' folder.

## Exercise 4
```sh
ansible-playbook exercise4.yml -i inventory/i.txt -vvv
```
