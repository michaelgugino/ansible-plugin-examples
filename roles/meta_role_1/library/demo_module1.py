#!/usr/bin/env python
# pylint: disable=missing-docstring
#
# Copyright 2017 Red Hat, Inc. and/or its affiliates
# and other contributors as indicated by the @author tags.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ansible.module_utils.basic import AnsibleModule

def run(module=None):
    # Access parameters
    myurl = module.params['url']
    result, stdout, stderr = module.run_command("ping -c1 %s" % myurl)
    if result:
        result = {'msg': stderr, 'failed': True, 'changed': False}
        # if you need to quick-exit and fail.
        module.fail_json(**result)
    msg = stdout
    msg += "; It worked! I pinged: {}".format(myurl)
    # This dictionary is for convenience
    result = {'changed': False, 'failed': False,
              'msg': msg}

    # how to exit a module the normal way.
    # **x means to 'unpack' a dictionary into keywords.
    # IE, (changed=False, failed=False, msg="...")
    module.exit_json(**result)


def main():
    '''Run this module'''
    # Boiler plate for defining params.
    module_args = dict(
        url=dict(aliases=['url', 'api'], required=True, type='str'),
        username=dict(type='str', required=False, no_log=True),
        password=dict(type='str', required=False, no_log=True)
    )

    # Need to instantiate a module object
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=False
    )
    run(module=module)


if __name__ == '__main__':
    main()
