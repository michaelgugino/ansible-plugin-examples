def meta_role_filter1(data="Empty?"):
    """Append something special to a string"""
    return str(data) + " Ansible rocks!"

class FilterModule(object):
    """ Custom ansible filter mapping """
    def filters(self):
        """ returns a mapping of filters to methods """
        return {"meta_role_filter1": meta_role_filter1}
