def meta_role_filter2(data="None?", other_var="none?"):
    """Append something special to a string"""
    return str(data) + " " + str(other_var) + " " + " Ansible rocks!"

class FilterModule(object):
    """ Custom ansible filter mapping """
    def filters(self):
        """ returns a mapping of filters to methods """
        return {"meta_role_filter2": meta_role_filter2}
