from ansible.plugins.action import ActionBase
from ansible import errors

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

class ActionModule(ActionBase):
    """A completely useless action"""
    def run(self, tmp=None, task_vars=None):
        """Run demo_action1 action plugin"""
        display.v("Running useless action: v")
        display.vvv("Running useless action: vvv")
        return {'changed': True, 'failed': False, 'msg': "I'm useless!"}
