from ansible.plugins.action import ActionBase
from ansible import errors

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

class ActionModule(ActionBase):
    """A somewhat useless action"""
    def run(self, tmp=None, task_vars=None):
        """Run demo_action3 action plugin"""

        # Need to run parent class's init method.
        result = super(ActionModule, self).run(tmp, task_vars)

        # Easy enough to access parameters, like so:
        param1 = self._task.args.get('param1', 'parma1_default')
        param2 = self._task.args.get('param2', 'parma2_default')

        # We can access variables that are not parameters as well.
        var1 = task_vars.get('tvar1')
        var2 = task_vars.get('tvar2')

        # You pretty much always want to template variables that you access
        # via task_vars.get...
        var3 = self._templar.template(var2)

        msg = "It worked!"
        return {'changed': True, 'failed': False, 'msg': msg,
                'param1': param1, 'param2': param2, 'var1': var1, 'var2': var2,
                'var3': var3}
