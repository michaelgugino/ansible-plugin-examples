from ansible.plugins.action import ActionBase
from ansible import errors

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

class ActionModule(ActionBase):
    """A completely useless action"""
    def run(self, tmp=None, task_vars=None):
        """Run demo_action2 action plugin"""
        return {
            'changed': True,
            'failed': False,
            'msg': "I'm useless!",
            'some_data': "something useful"}
