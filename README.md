This repository demonstrates how to make various
types of plugins for ansible.

# Prerequisites
You should be familiar with how to use ansible-playbook, constructing a basic
inventory, and how to write, read, and use basic roles and plays.

# Getting started
First things first, we want to ensure we have a working ansible environment.

## Install ansible

You can either install ansible via your distro's package manager or via
pip.

I prefer to use pip because it's nice to be able to standardized on an exact
release of ansible for your roles and plays; using pip with virtualenv allows
you to easily install multiple versions of ansible.

For installing via your package manager, see this document:
http://docs.ansible.com/ansible/latest/intro_installation.html#latest-release-via-yum

### virtualenv steps


#### python2
To install ansible in a a virtualenv, you'll need the following.

First, install python-setuptools which provides easy_install if necessary.
```sh
which easy_install || sudo yum install -y python-setuptools
```

Next, install pip via easy_install, if necessary.

```sh
which pip || sudo easy_install pip
pip install --user virtualenv
```

Alternatively, pip may be available in your distro as python-pip or python2-pip
if you prefer, same applies to virtualenv.

Now, we initialize a 'virtualenv' which is essentially setups up a stand-alone
python environment in a directory.

```sh
virtualenv venv
```

For more information on installing pip, visit: https://packaging.python.org/guides/installing-using-linux-tools/


#### python3
If you have python3 installed and wish to utilize python3 for development, you
can initialize a python3 virtualenv using the built-in venv module:
```sh
python3 -m venv venv
```

pip should also be available by default in python3.

### Install ansible via pip in venv

You can activate your new virtualenv like follows:
```sh
. venv/bin/activate
```
You should now see (venv) prepended on your terminal prompt.

Now that we have activated the virtualenv, we can use pip to install ansible.
We'll do this via requirements.txt, which is a common way for python projects
to declare dependencies.

```sh
pip install -r requirements.txt
```

This will install ansible and it's dependencies into the virtualenv.  These
packages won't interfere with the system installed packages.

If you need to leave your virtualenv at any point, you can type 'deactivate'.

## Test that ansible is working.
You can test that your installation is working correctly by executing the following:
```sh
ansible-playbook -i inventory/i.txt hello_world.yml
```

You should see output similar to the following:
```sh
PLAY [all] ***********

TASK [Gathering Facts] ******
ok: [localhost]
ok: [localhost2]

TASK [debug] ***********
ok: [localhost] => {
    "msg": "Hello world"
}
ok: [localhost2] => {
    "msg": "Hello world"
}

PLAY RECAP **************************
localhost                  : ok=2    changed=0    unreachable=0    failed=0   
localhost2                 : ok=2    changed=0    unreachable=0    failed=0
```
